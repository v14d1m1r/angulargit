import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class PostsService {
    constructor(private http: Http){
        console.log('PostsService Initialized!');
    }

    getPosts() {
        return this.http.get('https://api.chucknorris.io/jokes/random').map(res => res.json());
    }

    getCategories() {
        return this.http.get('https://api.chucknorris.io/jokes/categories').map(res => res.json());
    }

    getRandomJoke() {
        return this.http.get('https://api.chucknorris.io/jokes/random').map(res => res.json());
    }

    getRandomJokeByCategory(category) {
        return this.http.get("https://api.chucknorris.io/jokes/random?category="+category).map(res => res.json());
    }

    searchByQuery(query) {
        return this.http.get("https://api.chucknorris.io/jokes/search?query="+query).map(res => res.json());
    }
}