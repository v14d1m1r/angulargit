import { Component } from '@angular/core';
import { PostsService } from '../services/posts.service';

@Component({
  moduleId: module.id,  
  selector: 'home',
  templateUrl: 'home.component.html',
             providers: [PostsService]
})
export class HomeComponent  { 
  
  posts : Post[];
  joke : Joke[];
  jokes : string[];
  categories : string[];

  constructor(private postService: PostsService){
     
      this.jokes = [];
     
      this.postService.getPosts().subscribe(posts => {
        this.posts = posts;
      });

      this.postService.getCategories().subscribe(categories => {
        this.categories = categories;
      });

      this.postService.getRandomJoke().subscribe(joke => {
        this.joke = joke;
        this.jokes.unshift(joke.value);
      });
  }

  addRandomJoke(joke){
      joke = this.postService.getRandomJoke().subscribe(joke => {
        this.joke = joke;
        this.jokes.unshift(joke.value);
      });
      console.log(joke);
      this.jokes.unshift(joke.value);
  }


  deleteJoke(i:number){
      this.jokes.splice(i, 1);
  }

}

interface address{
    street: string;
    city: string;
    state: string;
}

interface Post {
    category : string;
    icon_url : string;
    id : string;
    url : string;
    value : string;
}

interface Joke {
    category : string;
    icon_url : string;
    id : string;
    url : string;
    value : string;
}