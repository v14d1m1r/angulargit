import { Component } from '@angular/core';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'categories',
  template: `<h1>Kategorije</h1>
    <div class="jokeCategory" *ngFor="let category of categories">
                <p class="jokeCategory"><button (click)="addRandomJokeByCategory(category)">{{category}}</button></p>
             </div>  
             <hr>
             <div *ngFor="let joke of jokes; let i = index">
                <p (click)="deleteJoke(i)">{{joke}}</p>
             </div>
  `, providers: [PostsService]
})
export class CategoriesComponent  { 
    category : string;
    categories : string[];
    jokes : string[];
    joke : string;
    
    constructor(private postService: PostsService) {
        this.postService.getCategories().subscribe(categories => {
        this.categories = categories;
      });
        this.jokes = []; 
        
    }


    addRandomJokeByCategory(category:string) {
        this.postService.getRandomJokeByCategory(category).subscribe(joke => {
            
            this.joke = joke;
            this.category = category;
            this.jokes.unshift(joke.value);
        });
    }

     deleteJoke(i:number){
      this.jokes.splice(i, 1);
  }
 }
