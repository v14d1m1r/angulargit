import { Component } from '@angular/core';
import { PostsService } from '../services/posts.service';

@Component({
    moduleId: module.id,
    selector: 'pretraga',
    template: `<h1>Pretraga</h1>
        <form>
            <input #search (keyup)="searchJokes(search.value)">&nbsp;Pritisni enter za pretragu
        </form>
       
        <div *ngFor="let q of results | slice:0:10; let i = index">    
           
                <p (click)="deleteJoke(i)">{{q.value}}</p>
            
        </div>
    `, providers: [PostsService]
})

export class PretragaComponent {
    query : string[];
    jokes : string[];
    joke : string;
    results: string[];


    constructor(private postService: PostsService) {
       this.jokes = [];
       this.results = [];
       this.query = [];
    }

    searchJokes(query:string[]) {
         
        this.postService.searchByQuery(query).subscribe(query => {
            this.query = query['result']; 
            this.results.unshift(query);  
           this.results = this.results[0]['result'];
           
        });
    }

    deleteJoke(i){
      this.results.splice(i, 1);
  }
    
}