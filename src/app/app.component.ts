import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<ul>
    <li><a routerLink='/'>Početna</a></li>
    <li><a routerLink='/categories'>Kategorije</a></li>
    <li><a routerLink='/pretraga'>Pretraga</a></li>
  </ul>
  <hr>
  <router-outlet></router-outlet>`,
})
export class AppComponent  { name = 'Angular'; }
