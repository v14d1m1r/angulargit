import {ModuleWithProviders}  from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { HomeComponent }  from './components/home.component';
import { CategoriesComponent }  from './components/categories.component';
import { PretragaComponent } from './components/pretraga.component';

const appRoutes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'categories',
        component: CategoriesComponent
    },
    {
        path: 'pretraga',
        component: PretragaComponent
    }

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);